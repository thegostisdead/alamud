# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2
from .info  import InfoEvent

class TakeEvent(Event2):
    NAME = "take"

    def perform(self):
        
        # debug 
        print("=" * 50 )
      
        for attr in dir(self.actor):
            print("self.actor.%s = %r" % (attr, getattr(self.actor, attr)))
        
        print("=" * 50 )
        print(self.actor.__dict__.keys())
        
        
        if not self.object.has_prop("takable"):
            self.add_prop("object-not-takable")
            return self.take_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        
        if self.object.id == 'pillule-bleu-000':
            self.inform("death")
            cont = self.actor.container()
            for x in list(self.actor.contents()):
                x.move_to(cont)
            self.actor.move_to(None)
            
        if self.object.id == 'pillule-rouge-000': 
            from mud.game import GAME
            loc = GAME.jump_for_player(self.actor)
            self.actor.move_to(loc)
            self.inform("teleportation")
            InfoEvent(self.actor).execute()
            
            
        self.object.move_to(self.actor)
        self.inform("take")

    def take_failed(self):
        self.fail()
        self.inform("take.failed")
