# Projet alamud    

Le projet **Alamud** est un projet destiné a nous faire utiliser git et maitriser les fichier de type yaml .     

## Le Groupe    


| Nom            |Classe                         | Email                             |
|----------------|-------------------------------|-----------------------------------|
|Jules Brossier  |1A42                           |jules.brossier@etu.univ-orleans.fr |
|Xavier Lemaire  |1A42                           | xavier.lemaire@etu.univ-orleans.fr|
|Dorian Hardy    |1A42                           | dorian.hardy@etu.univ-orelans.fr  |
